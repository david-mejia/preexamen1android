package com.example.preexamen1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    private EditText txtTrabajador;
    private Button btnIngresar;
    private Button btnSalir;
    private ReciboNomina recibo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        initComponents();
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtTrabajador.getText().toString().matches(""))
                    Toast.makeText(MainActivity.this, "Ingrese el nombre", Toast.LENGTH_SHORT).show();
                else{
                    String nombre = txtTrabajador.getText().toString();
                    Intent intent = new Intent(getApplicationContext(), ReciboNominaActivity.class);
                    intent.putExtra("trabajador", nombre);
                    startActivity(intent);
                }
            }
        });
    }

    public void initComponents(){
        txtTrabajador = (EditText) findViewById(R.id.txtTrabajador);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
        recibo = new ReciboNomina();
    }
}
